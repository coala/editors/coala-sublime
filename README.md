# SublimeLinter-coala

### Installation Instructions:

1) Install [SublimeLinter](http://www.sublimelinter.com/en/stable/installation.html)

2) Install `coala-sublime`

For Mac OSX:
`git clone https://gitlab.com/coala/editors/coala-sublime.git` in `/Library/Application Support/Sublime Text 3/Packages/`

For Ubuntu:
`git clone https://gitlab.com/coala/editors/coala-sublime.git` in `~/.config/sublime-text-3/Packages/`

![LineLengthBear](https://user-images.githubusercontent.com/15197846/61751380-23360600-ad76-11e9-9619-62c071deb932.gif)

![pep8](https://user-images.githubusercontent.com/15197846/61751437-4eb8f080-ad76-11e9-9e5f-3c21cde1000c.gif)
