from SublimeLinter.lint import Linter, PythonLinter


class coala(PythonLinter):
    cmd = ('coala --format')
    regex = r'^.*:line:(?P<line>\d+):\w+:(?P<col>\w+):\S+:(?P<message>(.+))'
    multiline = True
    defaults = {
        'selector': 'source.python',
    }
